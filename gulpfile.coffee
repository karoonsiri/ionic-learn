bower         = require 'bower'
sh            = require 'shelljs'
gulp          = require 'gulp'
gutil         = require 'gulp-util'
concat        = require 'gulp-concat'
sass          = require 'gulp-sass'
minifyCss     = require 'gulp-minify-css'
rename        = require 'gulp-rename'
coffee        = require 'gulp-coffee'
uglify        = require 'gulp-uglify'

paths = {
  sass: ['./scss/**/*.scss']
  lib:  ['./www/lib/**/*.js']
  app:  ['./coffee/**/*.coffee']
  appTemplates: ['./coffee/**/*.html']
}

gulp.task 'default', ['sass']

gulp.task 'build', ['sass', 'appjs', 'apptemplates']

gulp.task 'sass', (done) ->
  gulp.src './scss/ionic.app.scss'
    .pipe sass()
    .pipe gulp.dest('./www/css/')
    .pipe minifyCss(keepSpecialComments: 0)
    .pipe rename(extname: '.min.css')
    .pipe gulp.dest('./www/css/')
    .on('end', done)

  # Just return to avoid CoffeeScript's implicit returning the stream
  return

gulp.task 'watch', ->
  gulp.watch paths.sass, ['sass']
  gulp.watch paths.lib, ['ionicbundlejs']
  gulp.watch paths.app, ['appjs']
  gulp.watch paths.appTemplates, ['apptemplates']

gulp.task 'install', ['git-check'], ->
  bower.commands.install().on 'log', (data) ->
    gutil.log 'bower', gutil.colors.cyan(data.id), data.message

gulp.task 'git-check', (done) ->
  if !sh.which('git')
    console.log(
      '  ' + gutil.colors.red('Git is not installed.'),
      '\n  Git, the version control system, is required to download Ionic.',
      '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
      '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
    )
    process.exit(1)

  done()

gulp.task 'ionicbundlejs', ->
  srcPrefix = "./www/lib"
  gulp.src [
    "#{srcPrefix}/ionic/js/ionic.min.js",
    "#{srcPrefix}/angular/angular.min.js",
    "#{srcPrefix}/angular-animate/angular-animate.min.js",
    "#{srcPrefix}/angular-sanitize/angular-sanitize.min.js",
    "#{srcPrefix}/angular-ui-router/release/angular-ui-router.min.js",
    "#{srcPrefix}/ionic/js/ionic-angular.min.js"
  ]
    .pipe concat 'ionic.bundle.js'
    .pipe gulp.dest "#{srcPrefix}"

gulp.task 'appjs', ->
  gulp.src "./coffee/**/*.coffee"
  .pipe coffee().on 'error', gutil.log
  .pipe concat 'application.js'
  .pipe uglify()
  .pipe gulp.dest './www/js'

gulp.task 'apptemplates', ->
  gulp.src "./coffee/**/*.html"
    .pipe gulp.dest './www/templates/'
