accountCtrl = ($scope) ->
  $scope.settings = {
    enableFriends: true
  }

accountCtrl.$inject = ['$scope']

angular.module('ionicLearn.controllers').controller 'AccountCtrl', accountCtrl
