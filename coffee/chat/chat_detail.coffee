chatDetailCtrl = ($scope, $stateParams, Chats) ->
  $scope.chat = Chats.get $stateParams.chatId

chatDetailCtrl.$inject = ['$scope', '$stateParams', 'Chats']

angular.module('ionicLearn.controllers').controller 'ChatDetailCtrl', chatDetailCtrl
