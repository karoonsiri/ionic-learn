chatsCtrl = ($scope, Chats) ->
  $scope.chats = Chats.all()

  $scope.remove = (chat) ->
    Chats.remove(chat)

chatsCtrl.$inject = ['$scope', 'Chats']

angular.module('ionicLearn.controllers').controller 'ChatsCtrl', chatsCtrl
