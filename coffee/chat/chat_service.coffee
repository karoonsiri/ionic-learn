chatsService = ->
  # Might use a resource here that returns a JSON array

  # Some fake testing data
  chats = [
    {
      id: 0,
      name: 'Woramet Lertsiwanont',
      lastText: 'You on your way?',
      face: 'https://scontent-sin.xx.fbcdn.net/hphotos-xpf1/v/t1.0-9/10848012_10203041314624222_5757034977387534266_n.jpg?oh=fdc95bbecf0c804936bdf1ef168fc3b8&oe=55AC0A52&dl=1'
    },
    {
      id: 1,
      name: 'Karun Siritheerathamrong',
      lastText: "Hey, it's me",
      face: 'https://scontent-sin.xx.fbcdn.net/hphotos-xpf1/v/t1.0-9/10363738_10205891169982056_4145562274662376045_n.jpg?oh=30f74a7e2a775e3377d038dbbafa4ea8&oe=55A94DDF&dl=1'
    }
  ]

  return {
    all: ->
      return chats

    remove: (chat) ->
      chats.splice(chats.indexOf(chat), 1)

    get: (chatId) ->
      for chat in chats
        if chat.id == parseInt(chatId)
          return chat

      return null
  }

angular.module('ionicLearn.services').service 'Chats', chatsService
